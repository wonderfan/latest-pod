#!/usr/bin/env bash

function key(){
    ssh-keygen -C threewebcode
    cat ~/.ssh/id_rsa.pub
}

function ssh(){
    ls -al ~/.ssh
    cp ssh/* ~/.ssh/
    ls -al ~/.ssh
}

function config(){
  echo $1
  cd $1
  git config --local user.name threewebcode
  git config --local user.email magestore@outlook.com
}

function pf() {
  curl -L https://foundry.paradigm.xyz | bash
  sleep 2s
  export PATH=$PATH:/home/gitpod/.foundry/bin
  source /home/gitpod/.bashrc && foundryup
}

function setup() {
  rm -rf ~/.bitoai
  sudo curl https://alpha.bito.ai/downloads/cli/install.sh -fsSL | bash
  bito config -e
  sed -i 's/access_key: ""/access_key: "eyJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoidjFfMTYyNV85MDQ4NDBfNTY1OTU3X1R1ZSBPY3QgMTAgMDk6NDY6MTQgVVRDIDIwMjMifQ.BWA1t4pa7i9GZyl7Cbtmnqq3O4aIdM_Mf2Dsw4QRKKQ"/' ~/.bitoai/etc/bito-cli.yaml
  bito config --list
}

$@
